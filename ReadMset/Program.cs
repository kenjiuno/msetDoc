using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

namespace ReadMset {
    class Program {
        static void Main(string[] args) {
            if (args.Length < 1) {
                Console.Error.WriteLine("ReadMset P_EX100.mset");
                Environment.Exit(1);
            }
            using (FileStream fs = File.OpenRead(args[0])) {
                BinaryReader br = new BinaryReader(fs);
                foreach (BarEnt ent in ReadBar.Explode(fs)) {
                    if (ent.Format == 0x11) {
                        fs.Position = ent.BaseOffset + ent.Offset;
                        foreach (BarEnt ent2 in ReadBar.Explode(fs)) {
                            if (ent2.Format == 0x09) {
                                Console.WriteLine("@" + ent.Name + "#" + ent2.Name);
                                fs.Position = ent2.BaseOffset + ent2.Offset;
                                Motion o = new Motion(fs);

                                using (StreamWriter swr = new StreamWriter(ent.Name + "#" + ent2.Name + ".xml", false, Encoding.UTF8)) {
                                    using (XmlTextWriter xwr = new XmlTextWriter(swr)) {
                                        new XmlSerializer(typeof(Motion)).Serialize(xwr, o);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public class T1 {
        byte[] bin;

        public T1() { }

        public T1(BinaryReader br) {
            if ((this.bin = br.ReadBytes(8)).Length != 8) throw new EndOfStreamException();
        }

        [System.Xml.Serialization.XmlAttribute]
        public ushort JointIndex { get { return BitConverter.ToUInt16(bin, 0); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public ushort JointChannel { get { return BitConverter.ToUInt16(bin, 2); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public float Value { get { return BitConverter.ToSingle(bin, 4); } set { throw new NotSupportedException(); } }
    }

    public class T2 {
        byte[] bin;
        Motion parent;

        public T2() { }

        public T2(BinaryReader br, Motion parent) {
            if ((this.bin = br.ReadBytes(6)).Length != 6) throw new EndOfStreamException();

            this.parent = parent;
        }

        public T9[] T9 {
            get {
                return parent.ReadT9(IndexT9, CountT9);
            }
            set { throw new NotSupportedException(); }
        }

        [System.Xml.Serialization.XmlAttribute]
        public ushort JointIndex { get { return BitConverter.ToUInt16(bin, 0); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public ushort JointChannel { get { return bin[2]; } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public ushort CountT9 { get { return bin[3]; } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public ushort IndexT9 { get { return BitConverter.ToUInt16(bin, 4); } set { throw new NotSupportedException(); } }
    }

    public class T9 {
        byte[] bin;
        Motion parent;

        public T9() { }

        public T9(BinaryReader br, Motion parent) {
            if ((this.bin = br.ReadBytes(8)).Length != 8) throw new EndOfStreamException();

            this.parent = parent;
        }

        [System.Xml.Serialization.XmlAttribute]
        [System.Xml.Serialization.XmlIgnore]
        public ushort T11Off { get { return BitConverter.ToUInt16(bin, 0); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        [System.Xml.Serialization.XmlIgnore]
        public ushort T10Index { get { return BitConverter.ToUInt16(bin, 2); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        [System.Xml.Serialization.XmlIgnore]
        public ushort T12IndexA { get { return BitConverter.ToUInt16(bin, 4); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        [System.Xml.Serialization.XmlIgnore]
        public ushort T12IndexB { get { return BitConverter.ToUInt16(bin, 6); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public float Time { get { return parent.ReadT11(T11Off / 4); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public float Y { get { return parent.ReadT11(T10Index); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public float Parm1 { get { return parent.ReadT12(T12IndexA); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public float Parm2 { get { return parent.ReadT12(T12IndexB); } set { throw new NotSupportedException(); } }
    }

    public class T3 {
        byte[] bin;

        public T3() { }

        public T3(BinaryReader br) {
            if ((this.bin = br.ReadBytes(12)).Length != 12) throw new EndOfStreamException();
        }

        [System.Xml.Serialization.XmlAttribute]
        public byte Action { get { return bin[0]; } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public ushort b1 { get { return bin[1]; } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public ushort TargetJointIndex { get { return BitConverter.ToUInt16(bin, 2); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public ushort RefJointIndex { get { return BitConverter.ToUInt16(bin, 4); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public ushort w6 { get { return BitConverter.ToUInt16(bin, 6); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public uint dw8 { get { return BitConverter.ToUInt32(bin, 8); } set { throw new NotSupportedException(); } }
    }

    public class T4 {
        byte[] bin;

        public T4() { }

        public T4(BinaryReader br) {
            if ((this.bin = br.ReadBytes(4)).Length != 4) throw new EndOfStreamException();
        }

        [System.Xml.Serialization.XmlAttribute]
        public ushort JointIndex { get { return BitConverter.ToUInt16(bin, 0); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public ushort Flags { get { return BitConverter.ToUInt16(bin, 2); } set { throw new NotSupportedException(); } }
    }

    public class T5 {
        byte[] bin;

        public T5() { }

        public T5(BinaryReader br) {
            if ((this.bin = br.ReadBytes(64)).Length != 64) throw new EndOfStreamException();
        }

        [System.Xml.Serialization.XmlAttribute]
        public short JointIndex { get { return BitConverter.ToInt16(bin, 0); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public short ParentIndex { get { return BitConverter.ToInt16(bin, 4); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public float Sx { get { return BitConverter.ToSingle(bin, 16 + 4 * 0); } set { throw new NotSupportedException(); } }
        [System.Xml.Serialization.XmlAttribute]
        public float Sy { get { return BitConverter.ToSingle(bin, 16 + 4 * 1); } set { throw new NotSupportedException(); } }
        [System.Xml.Serialization.XmlAttribute]
        public float Sz { get { return BitConverter.ToSingle(bin, 16 + 4 * 2); } set { throw new NotSupportedException(); } }
        [System.Xml.Serialization.XmlAttribute]
        public float Sw { get { return BitConverter.ToSingle(bin, 16 + 4 * 3); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public float Rx { get { return BitConverter.ToSingle(bin, 32 + 4 * 0); } set { throw new NotSupportedException(); } }
        [System.Xml.Serialization.XmlAttribute]
        public float Ry { get { return BitConverter.ToSingle(bin, 32 + 4 * 1); } set { throw new NotSupportedException(); } }
        [System.Xml.Serialization.XmlAttribute]
        public float Rz { get { return BitConverter.ToSingle(bin, 32 + 4 * 2); } set { throw new NotSupportedException(); } }
        [System.Xml.Serialization.XmlAttribute]
        public float Rw { get { return BitConverter.ToSingle(bin, 32 + 4 * 3); } set { throw new NotSupportedException(); } }

        [System.Xml.Serialization.XmlAttribute]
        public float Tx { get { return BitConverter.ToSingle(bin, 48 + 4 * 0); } set { throw new NotSupportedException(); } }
        [System.Xml.Serialization.XmlAttribute]
        public float Ty { get { return BitConverter.ToSingle(bin, 48 + 4 * 1); } set { throw new NotSupportedException(); } }
        [System.Xml.Serialization.XmlAttribute]
        public float Tz { get { return BitConverter.ToSingle(bin, 48 + 4 * 2); } set { throw new NotSupportedException(); } }
        [System.Xml.Serialization.XmlAttribute]
        public float Tw { get { return BitConverter.ToSingle(bin, 48 + 4 * 3); } set { throw new NotSupportedException(); } }
    }

    public class Motion {
        Int64 baseoff;
        byte[] bin;

        Stream si;
        BinaryReader br;

        public Motion() { }

        public Motion(Stream si) {
            this.si = si;
            this.br = new BinaryReader(si);

            this.baseoff = si.Position;
            if ((this.bin = br.ReadBytes(0x90 + 0x70)).Length != (0x90 + 0x70)) throw new EndOfStreamException();
        }

        public T1[] T1 {
            get {
                T1[] al = new T1[cntT1];
                si.Position = baseoff + 0x90 + offT1;
                for (int x = 0; x < al.Length; x++) {
                    al[x] = new T1(br);
                }
                return al;
            }
            set { throw new NotSupportedException(); }
        }

        public T2[] T2 {
            get {
                T2[] al = new T2[cntT2];
                si.Position = baseoff + 0x90 + offT2;
                for (int x = 0; x < al.Length; x++) {
                    al[x] = new T2(br, this);
                }
                return al;
            }
            set { throw new NotSupportedException(); }
        }

        public T2[] T2x {
            get {
                T2[] al = new T2[cntT2x];
                si.Position = baseoff + 0x90 + offT2x;
                for (int x = 0; x < al.Length; x++) {
                    al[x] = new T2(br, this);
                }
                return al;
            }
            set { throw new NotSupportedException(); }
        }

        public T9[] ReadT9(int IndexT9, int CountT9) {
            T9[] al = new T9[CountT9];
            si.Position = baseoff + 0x90 + offT9 + 8 * IndexT9;
            for (int x = 0; x < al.Length; x++) {
                al[x] = new T9(br, this);
            }
            return al;
        }

        public float[] T11 {
            get {
                float[] al = new float[cntT11];
                si.Position = baseoff + 0x90 + offT11;
                for (int x = 0; x < al.Length; x++) {
                    al[x] = br.ReadSingle();
                }
                return al;
            }
            set { throw new NotSupportedException(); }
        }

        public float ReadT11(int i) {
            si.Position = baseoff + 0x90 + offT11 + 4 * i;
            return br.ReadSingle();
        }

        public float ReadT10(int i) {
            si.Position = baseoff + 0x90 + offT10 + 4 * i;
            return br.ReadSingle();
        }

        public float ReadT12(int i) {
            si.Position = baseoff + 0x90 + offT12 + 4 * i;
            return br.ReadSingle();
        }

        public T3[] T3 {
            get {
                T3[] al = new T3[cntT3];
                si.Position = baseoff + 0x90 + offT3;
                for (int x = 0; x < al.Length; x++) {
                    al[x] = new T3(br);
                }
                return al;
            }
            set { throw new NotSupportedException(); }
        }

        public T4[] T4 {
            get {
                T4[] al = new T4[cntT4];
                si.Position = baseoff + 0x90 + offT4;
                for (int x = 0; x < al.Length; x++) {
                    al[x] = new T4(br);
                }
                return al;
            }
            set { throw new NotSupportedException(); }
        }

        public T5[] T5 {
            get {
                T5[] al = new T5[cntT4 - startIndex2ndary];
                si.Position = baseoff + 0x90 + offT5;
                for (int x = 0; x < al.Length; x++) {
                    al[x] = new T5(br);
                }
                return al;
            }
            set { throw new NotSupportedException(); }
        }

        public ushort startIndex2ndary { get { return BitConverter.ToUInt16(bin, 0x90 + 0x10); } set { throw new NotSupportedException(); } }
        public ushort cntT4 { get { return BitConverter.ToUInt16(bin, 0x90 + 0x12); } set { throw new NotSupportedException(); } }
        public uint offT5 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x18); } set { throw new NotSupportedException(); } }
        public uint offT4 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x1C); } set { throw new NotSupportedException(); } }
        public uint cntT11 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x20); } set { throw new NotSupportedException(); } }
        public uint offT1 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x24); } set { throw new NotSupportedException(); } }
        public uint cntT1 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x28); } set { throw new NotSupportedException(); } }
        public uint offT2 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x30); } set { throw new NotSupportedException(); } }
        public uint cntT2 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x34); } set { throw new NotSupportedException(); } }
        public uint offT2x { get { return BitConverter.ToUInt32(bin, 0x90 + 0x38); } set { throw new NotSupportedException(); } }
        public uint cntT2x { get { return BitConverter.ToUInt32(bin, 0x90 + 0x3C); } set { throw new NotSupportedException(); } }
        public uint offT9 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x40); } set { throw new NotSupportedException(); } }
        public uint offT11 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x44); } set { throw new NotSupportedException(); } }
        public uint offT10 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x48); } set { throw new NotSupportedException(); } }
        public uint offT12 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x4C); } set { throw new NotSupportedException(); } }
        public uint offT3 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x50); } set { throw new NotSupportedException(); } }
        public uint cntT3 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x54); } set { throw new NotSupportedException(); } }
        public uint offT8 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x5C); } set { throw new NotSupportedException(); } }
        public uint offT7 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x60); } set { throw new NotSupportedException(); } }
        public uint cntT7 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x64); } set { throw new NotSupportedException(); } }
        public uint offT6 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x68); } set { throw new NotSupportedException(); } }
        public uint cntT6 { get { return BitConverter.ToUInt32(bin, 0x90 + 0x6C); } set { throw new NotSupportedException(); } }
    }

    public class BarEnt {
        Int64 baseoff;
        byte[] bin;

        public BarEnt(Int64 baseoff, BinaryReader br) {
            this.baseoff = baseoff;
            if ((this.bin = br.ReadBytes(16)).Length != 16) throw new EndOfStreamException();
        }

        public Int64 BaseOffset { get { return baseoff; } }

        public UInt32 Format { get { return BitConverter.ToUInt32(bin, 0); } }
        public String Name { get { return Encoding.ASCII.GetString(bin, 4, 4); } }
        public UInt32 Offset { get { return BitConverter.ToUInt32(bin, 8); } }
        public UInt32 Length { get { return BitConverter.ToUInt32(bin, 12); } }
    }
    public class ReadBar {
        public static BarEnt[] Explode(Stream si) {
            Int64 baseoff = si.Position;
            BinaryReader br = new BinaryReader(si);
            if (br.ReadByte() != (byte)'B') throw new InvalidDataException();
            if (br.ReadByte() != (byte)'A') throw new InvalidDataException();
            if (br.ReadByte() != (byte)'R') throw new InvalidDataException();
            if (br.ReadByte() != (byte)1) throw new InvalidDataException();
            uint cx = br.ReadUInt32();
            br.ReadUInt32();
            br.ReadUInt32();
            BarEnt[] al = new BarEnt[cx];
            for (uint x = 0; x < cx; x++) {
                al[x] = new BarEnt(baseoff, br);
            }
            return al;
        }
    }
}
