# kh2 mset format <!-- omit in toc -->

TOC:

- [Credits](#credits)
- [Terms](#terms)
  - [Bone](#bone)
  - [Joint](#joint)
  - [Skeleton](#skeleton)
  - [FK skeleton (defined in .mdlx)](#fk-skeleton-defined-in-mdlx)
  - [Inverse kinematics process](#inverse-kinematics-process)
  - [IK skeleton (defined in .anb)](#ik-skeleton-defined-in-anb)
- [Topics](#topics)
  - [MSET](#mset)
  - [ANB](#anb)
  - [About: IK data](#about-ik-data)
  - [FK/IK skeletons](#fkik-skeletons)
  - [Applying motion data into model (incomplete)](#applying-motion-data-into-model-incomplete)
  - [Research of KH2 IK (incomplete)](#research-of-kh2-ik-incomplete)
- [MSET header](#mset-header)
- [ANB header](#anb-header)
- [Motion header](#motion-header)
- [Table names](#table-names)
- [InitialPose (t1)](#initialpose-t1)
- [KeyInfo (t2)](#keyinfo-t2)
  - [User feedback:](#user-feedback)
- [KeyInfo for IK skeleton (t2x)](#keyinfo-for-ik-skeleton-t2x)
- [KeyIndex (t9)](#keyindex-t9)
  - [User feedback:](#user-feedback-1)
- [FrameTimes (t11)](#frametimes-t11)
- [KeyValues (t10)](#keyvalues-t10)
- [TangentValues (t12)](#tangentvalues-t12)
- [IKInfo (t3)](#ikinfo-t3)
- [BoneOrder (t4)](#boneorder-t4)
- [AxBone (t5)](#axbone-t5)
- [t6](#t6)
- [t7](#t7)
- [t8](#t8)

Known parsers:

- 010 Editor `.bt` (Binary Template) file: [Parser/mdlx/kh2-bar.bt · master · kenjiuno / khkh_xldM · GitLab](https://gitlab.com/kenjiuno/khkh_xldM/-/blob/master/Parser/mdlx/kh2-bar.bt)

## Credits

- User feedback: `MDLX_KEY_INFO`(t2) `MDLX_KEY_INDEX`(t9) by revel8n
- Recent documentation from https://openkh.dev/kh2/

## Terms

These terms are meant to explain my recognition, to help your understanding.
They may differ from standard terms definition.

### Bone

A 3D element having two (start and end) 3D positions, for example.

It is something like the following figure captured from Blender 2.49b

![](images/bone-in-blender.png)

![](images/bone-in-blender-2.png)

In mathematical, single bone consists from 4x4 Matrix having scale, rotation, and translation in relative 3D space.

In KH2, single bone consists from the following elements:

- currentBoneIndex
- parentBoneIndex
- scaling 3D vector
- rotation 3D vector
- translation 3D vector

### Joint

"Joint" is used as same as bone in this document, especially emphasizing start 3D position of bone.

### Skeleton

A collection of bones.

### FK skeleton (defined in .mdlx)

[Forward kinematics](https://en.wikipedia.org/wiki/Forward_kinematics) skeleton

![](images/tpose.png)

### Inverse kinematics process

A process to modify FK skeleton data.

The Cyclic Coordinate Descent (CCD) method is well known.

Usually modify **rotation** parameters of at least two bones, by assuming IK coordination system is translated into the single 2D plane.

And repeat this process around 4 times or more to get closer enough to the target position.

![](images/ik/ccd.png)

### IK skeleton (defined in .anb)

A FK skeleton used for [Inverse kinematics](https://en.wikipedia.org/wiki/Inverse_kinematics) process. This is a FK skeleton simply called as "IK skeleton" for its purpose meaning.

![](images/ikbone.png)

Notes:

- KH2 seems to append IK Matrix4x4 to after the end of FK Matrix4x4 in some situations. For example, [BoneOrder](#boneorder-t4) uses continous index numbering which means combining FK+IK items into one array.

## Topics

### MSET

Maybe _Motion Set_ (a set of motions). MSET contains one or more ANB.

### ANB

Maybe _ANimation Binary_. ANB contains single motion data.

ANB may be a standalone file, or be contained in mset file.

### About: IK data

IMO IK has 2 aspects: "a program implements IK process with CCD method or such" and "IK data".

To speak about IK in KH2, we need to share the recognition of "IK data" (The input information needed to compute IK result).

Fortunately Blender supports IK. So we can share the overview of "IK data" thru Blender's IK data representation on UI.

- [Introduction — Blender Manual](https://docs.blender.org/manual/en/2.90/animation/armatures/posing/bone_constraints/introduction.html)
- [Inverse Kinematics Constraint — Blender Manual](https://docs.blender.org/manual/en/2.90/animation/constraints/tracking/ik_solver.html)

In Blender:

- _Armature_ object represents a skeleton.
- _Armature_ object can be attached to a mesh object.
- IK is considered as a "constraint" object attached to _Armature_ object.
- One or more IK constraints can be attached to single _Armature_ object.

So, in `.anb` in KH2, it will have enough information to proceed this IK "constraint" process.

### FK/IK skeletons

FK skeleton (after IK processing). The data is defined in mdlx.

![](images/primarybone.png)

IK skeleton. The data is defined in [AxBone](#axbone-t5)(AxBone)

![](images/2ndarybone.png)

### Applying motion data into model (incomplete)

- T pose
  - By decoding mdlx.
- Apply [InitialPose](#initialpose-t1) to FK skeleton
  - Initialize pose which is best for motion.
- Apply [KeyInfo](#keyinfo-t2) to FK skeleton.
- Apply [KeyInfo for IK skeleton](#keyinfo-for-ik-skeleton-t2x) to IK skeleton.
  - Compute array of Matrix4x4 data.
- Apply IK ([IKInfo](#ikinfo-t3)).
  - e.g. Bond foots on floor.

T pose

![](images/tpose.png)

T pose

- +Apply fixed [InitialPose](#initialpose-t1)

![](images/tpose_f.png)

T pose

- +Apply fixed [InitialPose](#initialpose-t1)
- +Apply variables [KeyInfo](#keyinfo-t2)

![](images/tpose_fv.png)

T pose

- +Apply fixed [InitialPose](#initialpose-t1)
- +Apply variables [KeyInfo](#keyinfo-t2)
- +Apply experimental IK

![](images/tpose_fvk.png)

### Research of KH2 IK (incomplete)

IK skeleton:

![](images/ikbone.png)

IK info:

![](images/ikinf.png)

The rendering order is: `,#236,#11,#12,#13,#14,#15,` can be read from [BoneOrder](#boneorder).

- #236 is final target (at ground). joint#13 should meet joint#236.
- #11 can rotate for IK.
- #12 can rotate&move for IK.
- #13 can rotate&move for IK.
- #14 and #15 are for angle adjustment?

FK skeleton with experimental IK:

![](images/ikbonefig.png)

## MSET header

This docs uses P_EX100.mset in kh2fm for reference.

Header uses BAR (Binary ARchive?) format.

```
 ADDRESS   00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F   0123456789ABCDEF 
------------------------------------------------------------------------------
 00000000  42 41 52 01 47 03 00 00 00 00 00 00 01 00 00 00   BAR.G........... 
 00000010  11 00 00 00 41 30 30 30 80 34 00 00 9E 9A 00 00   ....A000.4...... 
 00000020  11 00 00 00 44 55 4D 4D 20 CF 00 00 00 00 00 00   ....DUMM ....... 
 00000030  11 00 00 00 41 30 30 30 20 CF 00 00 E6 26 00 00   ....A000 ....&.. 
 00000040  11 00 01 00 44 55 4D 4D 20 CF 00 00 00 00 00 00   ....DUMM ....... 
 00000050  11 00 00 00 41 30 30 31 10 F6 00 00 12 62 00 00   ....A001.....b.. 
 00000060  11 00 01 00 44 55 4D 4D 20 CF 00 00 00 00 00 00   ....DUMM ....... 
 00000070  11 00 00 00 41 30 30 31 30 58 01 00 12 5F 00 00   ....A0010X..._.. 
```

Trace 1st A000. It is an ANB.

```
 00000010  11 00 00 00 41 30 30 30 80 34 00 00 9E 9A 00 00   ....A000.4...... 
```

## ANB header

Trace 1st entry. It is motion data. A000 is a Sora's slow walking motion (with keyblade placeholder).

```
 ADDRESS   00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F   0123456789ABCDEF 
------------------------------------------------------------------------------
 00003480  42 41 52 01 02 00 00 00 00 00 00 00 00 00 00 00   BAR............. 
 00003490  09 00 00 00 41 30 30 30 30 00 00 00 30 9A 00 00   ....A0000...0... 
 000034A0  10 00 00 00 41 30 30 30 60 9A 00 00 3E 00 00 00   ....A000`...>... 
```

Trace 1st entry.

```
 00003490  09 00 00 00 41 30 30 30 30 00 00 00 30 9A 00 00   ....A0000...0... 
```

## Motion header

![](images/mseth3.png)

Skip first 0x90 bytes. It is used by game system to place memory pointers.

The t1-t11 are tables.

At 0x00000000, it is `uint32 format`.

- `0` is interpolated format explained at this doc.
- `1` is RAW format using pre-built array of Matrix4x4, with different header format. It is now available at OpenKh project https://openkh.dev/kh2/file/anb/anb.html

## Table names

Recently OpenKh communicty members have parsed anb file structure. See also https://openkh.dev/kh2/file/anb/motion.html

Here is a table name mapping:

Numbered | Revealed name | OpenKh
---|---|:-:
T1 | InitialPose | [See](https://openkh.dev/kh2/file/anb/motion.html#initial-pose)
T2 | FCurvesForward | [See](https://openkh.dev/kh2/file/anb/motion.html#f-curves)
T2x | FCurvesInverse | [See](https://openkh.dev/kh2/file/anb/motion.html#f-curves)
T3 | Constraints | [See](https://openkh.dev/kh2/file/anb/motion.html#constraints)
T4 | JointIndices | [See](https://openkh.dev/kh2/file/anb/motion.html#joint-definition)
T5 | IKHelpers | [See](https://openkh.dev/kh2/file/anb/motion.html#inverse-kinematic-helper)
T6 | ExpressionNodes | [See](https://openkh.dev/kh2/file/anb/motion.html#expression-nodes)
T7 | Expressions | [See](https://openkh.dev/kh2/file/anb/motion.html#expressions)
T8 | Limiters | [See](https://openkh.dev/kh2/file/anb/motion.html#constraint-limiters)
T9 | FCurveKeys | [See](https://openkh.dev/kh2/file/anb/motion.html#f-curve-keys)
T10 | KeyValues | [See](https://openkh.dev/kh2/file/anb/motion.html#key-values)
T11 | KeyTimes | [See](https://openkh.dev/kh2/file/anb/motion.html#key-times)
T12 | KeyTangents | [See](https://openkh.dev/kh2/file/anb/motion.html#key-tangents)

t13: off 0xa0

## InitialPose (t1)

The **InitialPose** defines **initial pose** for its motion (walking, running, and standing, etc). It will destroy T pose defined by mdlx.

Data body:

![](images/t1hdr.png)

| Topic         | Description                                                                                                                                                                                                                     |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| joint#        | Apply fixed value for ax in joint specified by joint#.                                                                                                                                                                          |
| joint channel | 0 = Modify Scale.x<br>1 = Modify Scale.y<br>2 = Modify Scale.z<br>3 = Modify Rotate.x<br>4 = Modify Rotate.y<br>5 = Modify Rotate.z<br>6 = Modify Translate.x<br>7 = Modify Translate.y<br>8 = Modify Translate.z<br>Others = ? |
| value         | A decimal value to define initial pose.                                                                                                                                                                                         |

Reading example:

![](images/t1read.png)

## KeyInfo (t2)

The **KeyInfo animates model** by changing SRT (scale, rotation, translation) vectors.

**KeyInfo** modifies joints in FK skeleton, while [t2x](#keyinfo-for-ik-skeleton-t2x) changes IK skeleton.

**KeyInfo** contains one or more [KeyIndex](#keyindex-t9) as children.

Data body:

![](images/t2hdr.png)

```c
struct MDLX_KEY_INFO
{
    uint16 jointIndex;
    uint8  keyType  : 4;
    uint8  preType  : 2;
    uint8  postType : 2;
    uint8  keyCount;
    uint16 keyStart;
};

// pre and post type determine how the key value is handled when the current frame time is less than or greater than the start and end frame times respectively.

// The values can be:
// cycle type:
// 0 - first/last key
// 1 - subtractive/additive
// 2 - repeat
// 3 - zero
```

| Legend              | Description                                                                                                                                                       |
| ------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| jointIndex          | joint number.                                                                                                                                                     |
| keyType             | joint channel.<br>3 = Modify Rotate.x<br>4 = Modify Rotate.y<br>5 = Modify Rotate.z<br>6 = Modify Translate.x<br>7 = Modify Translate.y<br>8 = Modify Translate.z |
| preType<br>postType | 0 - first/last key<br>1 - subtractive/additive<br>2 - repeat<br>3 - zero<br>                                                                                      |
| keyCount            | count [KeyIndex](#keyindex-t9)                                                                                                                                    |
| keyStart            | 0 based start index of [KeyIndex](#keyindex-t9)                                                                                                                   |

### User feedback:

```c
struct MDLX_KEY_INFO
{
    uint16 jointIndex;
    uint8  keyType  : 4;
    uint8  preType  : 2;
    uint8  postType : 2;
    uint8  keyCount;
    uint16 keyStart;
};
```

![](images/struc_mdlx_key_info.png)
```
keyType => joint channel

pre and post type determine how the key value is handled when the current 
frame time is less than or greater than the start and end frame times respectively.

The values can be:
cycle type:
0 - first/last key
1 - subtractive/additive
2 - repeat
3 - zero
```

Reading example:

**t2** is "#000.Rx", "#000.Rz", and so on.
[KeyIndex](#keyindex-t9) starts complex structure "#2|0.00|0.43|0.00|0.00". The decimal values are in other tables: "[KeyIndex](#keyindex-t9)|[FrameTimes](#frametimes-t11)|[KeyValues](#keyvalues-t10)|[TangentValues](#tangentvalues-t12)|t12".

![](images/t2read.png)

## KeyInfo for IK skeleton (t2x)

**t2x** is same as [KeyInfo](#keyinfo-t2).

The difference is that **t2x** modifies joints in **IK skeleton** defined inside this motion data.

Data body:

```
ADDRESS  00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 0123456789ABCDEF
-------  -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------
00001414 08 00|03|08|4F 00                               ....O.          
0000141A 08 00 04 08 57 00                               ....W.          
00001420 08 00 05 09 5F 00                               ...._.          
00001426 08 00 06 05 68 00                               ....h.          
0000142C 08 00 07 06 6D 00                               ....m.          
00001432 08 00 08 03 73 00                               ....s.          
00001438 09 00 03 0A C1 00                               ......          
0000143E 09 00 04 08 CB 00                               ......          
00001444 09 00 05 08 D3 00                               ......          
0000144A 09 00 06 05 DB 00                               ......          
00001450 09 00 07 07 E0 00                               ......          
00001456 09 00 08 04 E7 00                               ......          
0000145C 03 00 03 0C 40 02                               ....@.          
00001462 03 00 04 0A 4C 02                               ....L.          
00001468 03 00 05 0F 56 02                               ....V.          
0000146E 04 00 03 0C 40 02                               ....@.          
```

Note: Joint# is zero-based number against IK skeleton.

## KeyIndex (t9)

The **KeyIndex** defines **timeline table**. referenced by [KeyInfo](#keyinfo-t2).

KeyIndex has pointers to [FrameTimes](#frametimes-t11), [KeyValues](#keyvalues-t10), and [TangentValues](#tangentvalues-t12).

Data body:

![](images/t9hdr.png)

### User feedback:

```c
struct MDLX_KEY_INDEX
{
    uint16 interpolation :  2;
    uint16 timeIndex     : 14;
    uint16 valueIndex;
    uint16 tangentIndex0; // in-tangent
    uint16 tangentIndex1; // out-tangent
};
```

![](images/struc_mdlx_key_index.png)

```
As you might be able to tell, i found out why the timeOffset is usually not an even multiple of 4.  
The bottom 2 bits are used to specify the type of interpolation to use for keys.  
Technically you can mask the value off as is currently being done in order to use it as an offset into the frame time values; 
i marked it as a bit mask in order to use the value as an index into the array.
The interpolation values can be
interpolation type:
0 - step
1 - linear
2 - hermite
3 - zero
Although i have not seen many instances where the interpolation type is not hermite.
-----------------------------------
t11: frame times
t10: key values
t12: tangent values
-----------------------------------
```

## FrameTimes (t11)

The **t11** contains float values. Used by [KeyIndex](#keyindex-t9).

Data body:

```
 ADDRESS 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 0123456789ABCDEF
-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------
000052A8 00 00 00 00                                     ....            
000052AC 00 00 00 40                                     ...@            
000052B0 00 00 80 40                                     ...@            
000052B4 00 00 C0 40                                     ...@            
000052B8 00 00 00 41                                     ...A            
000052BC 00 00 20 41                                     .. A            
000052C0 00 00 40 41                                     ..@A            
000052C4 00 00 60 41                                     ..`A            
000052C8 00 00 80 41                                     ...A            
000052CC 00 00 90 41                                     ...A            
000052D0 00 00 C0 41                                     ...A            
000052D4 00 00 D0 41                                     ...A            
000052D8 00 00 F0 41                                     ...A            
000052DC 00 00 00 42                                     ...B            
000052E0 00 00 08 42                                     ...B            
000052E4 00 00 10 42                                     ...B            
```

## KeyValues (t10)

The **KeyValues** contains float values. Used by [KeyIndex](#keyindex-t9).

Data body:

```
 ADDRESS 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 0123456789ABCDEF
-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------
000054E4 78 10 DD 3E                                     x..>            
000054E8 CB EE E0 3E                                     ...>            
000054EC 6B 15 EF 3E                                     k..>            
000054F0 00 07 F6 3E                                     ...>            
000054F4 B0 B3 E1 3E                                     ...>            
000054F8 F3 F1 CA 3E                                     ...>            
000054FC C7 BC C7 3E                                     ...>            
00005500 F6 E9 BD 3E                                     ...>            
00005504 79 A2 C7 3E                                     y..>            
00005508 79 10 DD 3E                                     y..>            
0000550C F2 4E C4 3D                                     .N.=            
00005510 38 B9 AF 3D                                     8..=            
00005514 07 CC 6F 3D                                     ..o=            
00005518 EB 54 CC 3D                                     .T.=            
0000551C 41 BF 20 3E                                     A. >            
00005520 B1 48 43 3E                                     .HC>            
```

## TangentValues (t12)

The **TangentValues** contains float values. Used by [KeyIndex](#keyindex-t9).

Data body:

```
 ADDRESS 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 0123456789ABCDEF
-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------
00007154 29 A5 0F B9                                     )...            
00007158 DC 32 D2 3A                                     .2.:            
0000715C AC D1 29 3B                                     ..);            
00007160 7B A2 21 BB                                     {.!.            
00007164 FA 4D 91 B9                                     .M..            
00007168 1E 0F 86 BB                                     ....            
0000716C FB A7 07 BB                                     ....            
00007170 71 74 DF B9                                     qt..            
00007174 1D DC 0A 3B                                     ...;            
00007178 3A 41 5E B9                                     :A^.            
0000717C 11 26 C5 B9                                     .&..            
00007180 29 0C AE BA                                     )...            
00007184 39 82 E2 3A                                     9..:            
00007188 31 C7 AB 3A                                     1..:            
0000718C 87 AE 43 3B                                     ..C;            
00007190 69 FD 83 BA                                     i...            
```

## IKInfo (t3)

The **IKInfo** may be info for **Inverse Kinematics**. The detail is still unknown.

Data body:

![](images/t3hdr.png)

Format:

| Legend        | Description                                                                                                                                         |
| ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| action        | Maybe:<br>0 = Copy Matrix from ref joint# to target joint#.<br>1 = Copy Rotation from  ref joint# to target joint#.<br>3 = IK target is ref joint#. |
| target joint# | Current processing joint#. It is usually t4.w0.                                                                                                     |
| ref joint#    | A joint# to be fetched information.                                                                                                                 |

## BoneOrder (t4)

The **BoneOrder** defines **the order to process bones**.

Data body:

**4 bytes** per entry.

```
 ADDRESS 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 0123456789ABCDEF
-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------
00009580 00 00|00 00                                     ....            
00009584 01 00 00 00                                     ....            
00009588 02 00 00 00                                     ....            
0000958C 03 00 00 00                                     ....            
00009590 04 00 00 00                                     ....            
00009594 05 00 00 00                                     ....            
00009598 06 00 00 00                                     ....            
0000959C 07 00 00 00                                     ....            
000095A0 08 00 00 00                                     ....            
000095A4 09 00 00 00                                     ....            
000095A8 E4 00 00 00                                     ....            
000095AC EA 00 00 00                                     ....            
000095B0 0A 00 00 00                                     ....            
000095B4 EC 00 00 00                                     ....            
000095B8 0B 00 02 00                                     ....            
000095BC 0C 00 20 00                                     .. .            
```

Format:

```c
struct BoneOrder {
  UInt16 w0; // Joint# to proceed.
             // joint# from 0x00 to 0xE3 is in FK skeleton (mdlx).
             // joint# from 0xE4 to 0xEE is in IK skeleton (t5).
  UInt16 w2; // Flags? still unknown.
             // 0x01 ... IK1a flag?
             // 0x02 ... w0 is IK joint1
             // 0x20 ... w0 is IK joint2
             // 0x04 ... Proceed IK computer.
             // 0x08 ... IK3a flag?
};
```

## AxBone (t5)

It defines single IK skeleton.

**64 bytes** per entry:

```
[[  CurJoint#, ParentJoint#, ?, ?],
 [    Scale.x, y, z, w],
 [   Rotate.x, y, z, w],
 [Translate.x, y, z, w]]
```

Data body:

```
 ADDRESS   00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F   0123456789ABCDEF 
------------------------------------------------------------------------------
 00009350  E4 00 00 00 FF FF FF FF 00 00 00 00 00 00 00 00   ................ 
 00009360  00 00 80 3F 00 00 80 3F 00 00 80 3F 00 00 00 00   ...?...?...?.... 
 00009370  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................ 
 00009380  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................ 

 00009390  E5 00 00 00 E4 00 00 00 00 00 00 00 00 00 00 00   ................ 
 000093A0  00 00 80 3F 00 00 80 3F 00 00 80 3F 00 00 00 00   ...?...?...?.... 
 000093B0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................ 
 000093C0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................ 

 000093D0  E6 00 00 00 E5 00 00 00 00 00 00 00 00 00 00 00   ................ 
 000093E0  00 00 80 3F 00 00 80 3F 00 00 80 3F 00 00 00 00   ...?...?...?.... 
 000093F0  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................ 
 00009400  00 00 00 00 54 C3 CC 42 00 00 A0 C0 00 00 00 00   ....T..B........ 
```

## t6

Unknown.

## t7

Unknown

## t8

Unknown

